"""
====================================================================================================================
Collection of functions to estimate the Gravitational forces and accelerations (:mod:`fireworks.nbodylib.dynamics`)
====================================================================================================================

This module contains a collection of functions to estimate acceleration due to
gravitational  forces.

Each method implemented in this module should follow the input-output structure show for the
template function  :func:`~acceleration_estimate_template`:

Every function needs to have two input parameters:

    - particles, that is an instance of the class :class:`~fireworks.particles.Particles`
    - softening, it is the gravitational softening. The parameters need to be included even
        if the function is not using it. Use a default value of 0.

The function needs to return a tuple containing three elements:

    - acc, the acceleration estimated for each particle, it needs to be a Nx3 numpy array,
        this element is mandatory it cannot be 0.
    - jerk, time derivative of the acceleration, it is an optional value, if the method
        does not estimate it, just set this element to None. If it is not None, it has
        to be a Nx3 numpy array.
    - pot, gravitational potential at the position of each particle. it is an optional value, if the method
        does not estimate it, just set this element to None. If it is not None, it has
        to be a Nx1 numpy array.


"""
from typing import Optional, Tuple
import numpy as np
import numpy.typing as npt
from ..particles import Particles

try:
    import pyfalcon
    pyfalcon_load=True
except:
    pyfalcon_load=False

def acceleration_estimate_template(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    This an empty functions that can be used as a basic template for
    implementing the other functions to estimate the gravitational acceleration.
    Every function of this kind needs to have two input parameters:

        - particles, that is an instance of the class :class:`~fireworks.particles.Particles`
        - softening, it is the gravitational softening. The parameters need to be included even
          if the function is not using it. Use a default value of 0.

    The function needs to return a tuple containing three elements:

        - acc, the acceleration estimated for each particle, it needs to be a Nx3 numpy array,
            this element is mandatory it cannot be 0.
        - jerk, time derivative of the acceleration, it is an optional value, if the method
            does not estimate it, just set this element to None. If it is not None, it has
            to be a Nx3 numpy array.
        - pot, gravitational potential at the position of each particle. it is an optional value, if the method
            does not estimate it, just set this element to None. If it is not None, it has
            to be a Nx1 numpy array.

    :param particles: An instance of the class Particles
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - acc, Nx3 numpy array storing the acceleration for each particle
        - jerk, Nx3 numpy array storing the time derivative of the acceleration, can be set to None
        - pot, Nx1 numpy array storing the potential at each particle position, can be set to None
    """

    acc  = np.zeros(len(particles))
    jerk = None
    pot = None

    return (acc,jerk,pot)

def acceleration(particles,i,j):

    a = np.zeros(3)

    dx = particles.pos[i,0]-particles.pos[j,0]
    dy = particles.pos[i,1]-particles.pos[j,1]
    dz = particles.pos[i,2]-particles.pos[j,2]

    r = np.sqrt(dx**2+dy**2+dz**2)

    a[0] = - particles.mass[j] * dx / (r**3)
    a[1] = - particles.mass[j] * dy / (r**3)
    a[2] = - particles.mass[j] * dz / (r**3)

    return a


def acceleration_direct(particles:Particles, softening: float =0.):

    acc = np.zeros((len(particles),3))
    jerk = None
    pot = None

    for i in range(len(particles)):
        for j in range(len(particles)):
            if j != i:
                acc[i] += acceleration(particles,i,j)

    return (acc, jerk, pot)

def acceleration_direct_vectorized(particles:Particles, softening: float =0.):
    #particles.pos and particles.vel are Nx3 matrices. particles.mass is a Nx1 vector

    acc = np.zeros((len(particles),3))
    jerk = None
    pot = None

    #first method (ours):
    pos_copy = np.copy(particles.pos)
    pos_new = pos_copy[:, np.newaxis]

    pos_diff = particles.pos - pos_new

    #alternative:
    #pos_diff = particles.pos[:, np.newaxis, :] - particles.pos[np.newaxis, :, :]

    #particles.pos[:, np.newaxis, :] has dimensions (N, 1, 3) and converts each row vector of shape (3,) into 1x3 vector
    #particles.pos[np.newaxis, :, :] has dimensions (1, N, 3) and converts each row vector of shape (3,) into 3x1 vector
    #pos_diff has dimension (N,N,3): one Nx3 matrix for each particle
    #containing the difference in coordinates (x,y,z are the columns) of that particle with all the others

    dist = np.linalg.norm(particles.pos - pos_new, axis=2) #dist is a NxN symmetric matrix with zeros on the diagonal
    np.fill_diagonal(dist, np.inf) #set diagonal elements to a very large number to avoid division by zero

    r_inv = 1.0 / dist**3

    acc[:,0] = np.sum(particles.mass * pos_diff[:,:,0] * r_inv, axis=1)
    #pos_diff[:,:,0] is a NxN matrix in which thw ij entry is x_i - x_j
    #the summation along axis 1 (summing the elements of each row) collapses the contributions from different particles along x
    #resulting in a final array of accelerations for each particle
    acc[:,1] = np.sum(particles.mass * pos_diff[:,:,1] * r_inv, axis=1)
    acc[:,2] = np.sum(particles.mass * pos_diff[:,:,2] * r_inv, axis=1)

    return acc, jerk, pot

def acceleration_direct_vectorised2(particles:Particles, softening: float =0.):
    #particles.pos and particles.vel are Nx3 matrices. particles.mass is a Nx1 vector

    acc = np.zeros((len(particles),3))
    jerk = None
    pot = None

    P = particles.pos
    Px,Py,Pz = np.hsplit(P,3) #split the matrix in three vectors corresponding to the spatial coordinates
    PT = np.array(P).T #transpose matrix
    PxT,PyT,PzT = np.vsplit(PT,3)

    Dx = PxT-Px #this gives a NxN matrix for the x coordinate where each entry is x_i - x_j.
    # The matrix is antisymmetric with zeros along the diagonal.
    Dy = PyT-Py
    Dz = PzT-Pz

    R = np.sqrt(np.square(Dx)+np.square(Dy)+np.square(Dz))
    R3 = R**3
    N = len(particles)
    ret = np.zeros((N,N))

    # ret is a NxN matrix of zeros, I need it to avoid the divisions by zero in the next passage
    # Whenever  R3!==0 I do the division, in the other case don't divide and the result is set to zero.
    acc[:,0] = - particles.mass @ np.divide(Dx, R3, out=ret, where= R3!=0)
    acc[:,1] = - particles.mass @ np.divide(Dy, R3, out=ret, where= R3!=0)
    acc[:,2] = - particles.mass @ np.divide(Dz, R3, out=ret, where= R3!=0)

    return acc, jerk, pot



def acceleration_pyfalcon(particles: Particles, softening: float =0.) \
        -> Tuple[npt.NDArray[np.float64],Optional[npt.NDArray[np.float64]],Optional[npt.NDArray[np.float64]]]:
    """
    Estimate the acceleration following the fast-multipole gravity Dehnen2002 solver (https://arxiv.org/pdf/astro-ph/0202512.pdf)
    as implemented in pyfalcon (https://github.com/GalacticDynamics-Oxford/pyfalcon)

    :param particles: An instance of the class Particles
    :param softening: Softening parameter
    :return: A tuple with 3 elements:

        - Acceleration: a NX3 numpy array containing the acceleration for each particle
        - Jerk: None, the jerk is not estimated
        - Pot: a Nx1 numpy array containing the gravitational potential at each particle position
    """

    if not pyfalcon_load: return ImportError("Pyfalcon is not available")

    acc, pot = pyfalcon.gravity(particles.pos,particles.mass,softening)
    jerk = None

    return acc, jerk, pot

# def acceleration_jerk_direct(particles: Particles):

#     jerk = np.zeros((len(particles), 3))

